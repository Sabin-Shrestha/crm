<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CRM</title>

    @include('includes.css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('includes.navbar')
    @include('includes.sidebar')
    @include('includes.footer')

</div>
{{--<div class="row">--}}
{{--    <div class="container-fluid">--}}
{{--        <div class="col-lg-12">--}}
{{--            @include('includes.navbar')--}}
{{--        </div>--}}
{{--        <div class="col-lg-4">--}}
{{--            @include('includes.sidebar')--}}
{{--        </div>--}}
{{--        <div class="col-lg-8">--}}
{{--            @yield('content')--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}


</body>
@include('includes.script')
@yield('script')
</html>
