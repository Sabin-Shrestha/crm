@extends('layouts.masterlayout')

@section('content')

<h1>Contact Page</h1>
<div class="container-fluid">
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <form method="post" enctype="multipart/form-data">

                                    @csrf

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name">User Name</label>
                                                <input type="text" class="form-control" name="username"
                                                       id="username"></input>
                                                <span
                                                    style="color: #ff0040">@error('username'){{$message}}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" name="email"
                                                       id="email"></input>
                                                <span
                                                    style="color: #ff0040">@error('email'){{$message}}@enderror</span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="message">Message</label>
                                                <input type="text" class="form-control" name="message"
                                                       id="message"></input>
                                                <span
                                                    style="color: #ff0040">@error('message'){{$message}}@enderror</span>
                                            </div>
                                        </div>



                                    <button id="submit" class="btn btn-primary btn-sm"> Submit</button>

                                </form>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection




